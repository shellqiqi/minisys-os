# microAptiv_UP makefile for MIPSfpga

ifndef MIPS_ELF_ROOT
$(error MIPS_ELF_ROOT must be set to point to toolkit installation root)
endif

CC = mips-mti-elf-gcc
LD = mips-mti-elf-ld
OD = mips-mti-elf-objdump
OC = mips-mti-elf-objcopy
SZ = mips-mti-elf-size

# Imagination CodeScape toolchain
ifneq (,$(wildcard $(MIPS_ELF_ROOT)/bin/mips-mti-elf-gcc))
	CC = $(MIPS_ELF_ROOT)/bin/mips-mti-elf-gcc
	LD = $(MIPS_ELF_ROOT)/bin/mips-mti-elf-gcc
	OD = $(MIPS_ELF_ROOT)/bin/mips-mti-elf-objdump
	OC = $(MIPS_ELF_ROOT)/bin/mips-mti-elf-objcopy
	SZ = $(MIPS_ELF_ROOT)/bin/mips-mti-elf-size
endif

# Mentor CodeSourcery toolchain
ifneq (,$(wildcard $(MIPS_ELF_ROOT)/bin/mips-sde-elf-gcc))
	CC = $(MIPS_ELF_ROOT)/bin/mips-sde-elf-gcc
	LD = $(MIPS_ELF_ROOT)/bin/mips-sde-elf-gcc
	OD = $(MIPS_ELF_ROOT)/bin/mips-sde-elf-objdump
	OC = $(MIPS_ELF_ROOT)/bin/mips-sde-elf-objcopy
	SZ = $(MIPS_ELF_ROOT)/bin/mips-sde-elf-size
endif

CFLAGS  = -EL -g -march=m14kc -msoft-float -O1 -I . -G0
LDFLAGS = -EL -nostartfiles -N -T scse0_3.lds -G0

OBJDIRS := boot lib drivers init kern mm

ASOURCES = $(foreach dir, $(OBJDIRS), $(wildcard $(dir)/*.S))


CSOURCES = $(foreach dir, $(OBJDIRS), $(wildcard $(dir)/*.c))


COBJECTS = $(CSOURCES:.c=.o) user/mytest.x
AOBJECTS = $(ASOURCES:.S=.o)

all: program.elf

program.elf : $(AOBJECTS) $(COBJECTS)
	$(LD)  $(LDFLAGS) $(AOBJECTS) $(COBJECTS) -o program.elf
	$(OC) --remove-section .MIPS.abiflags --remove-section .reginfo program.elf
	$(SZ) program.elf
	$(OD) -D -l -t program.elf > program.dis
	$(OD) -D program.elf > program.txt
	$(OC) program.elf -O srec program.rec

.c.o:
	$(CC) -c $(CFLAGS) $< -o $@

.S.o:
	$(CC) -c $(CFLAGS) $< -o $@

user/mytest.x:
	cd user && make

clean:
	rm -f $(foreach dir, $(OBJDIRS),$(wildcard $(dir)/*.o))
	rm -f program.elf
	rm -f program.map
	rm -f program.dis
	rm -f program.rec
	rm -f *.txt
	cd user && make clean