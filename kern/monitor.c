#include "monitor.h"

// Simple command-line kernel monitor useful for
// controlling the kernel and exploring the system interactively.

#include <inc/printf.h>
#include <inc/string.h>

#include <kern/console.h>
// #include <kern/kdebug.h>

#define CMDBUF_SIZE	80	// enough for one VGA text line

struct Command {
    const char *name;
    const char *desc;
    // return -1 to force monitor to exit
    int (*func)(int argc, char** argv, struct Trapframe* tf);
};

static struct Command commands[] = {
    { "help", "Display this list of commands", mon_help },
    { "backtrace", "Display a listing of function call frames", mon_backtrace },
    { "kerninfo", "Display information about the kernel", mon_kerninfo },
};

/***** Implementations of basic kernel monitor commands *****/

int
mon_help(int argc, char **argv, struct Trapframe *tf)
{
    int i;

    for (i = 0; i < ARRAY_SIZE(commands); i++)
        printf("%s - %s\n", commands[i].name, commands[i].desc);
    return 0;
}

int
mon_kerninfo(int argc, char **argv, struct Trapframe *tf)
{
    // extern char _start[], entry[], etext[], edata[], end[];

    // printf("Special kernel symbols:\n");
    // printf("  _start                  %08x (phys)\n", _start);
    // printf("  entry  %08x (virt)  %08x (phys)\n", entry, entry - KERNBASE);
    // printf("  etext  %08x (virt)  %08x (phys)\n", etext, etext - KERNBASE);
    // printf("  edata  %08x (virt)  %08x (phys)\n", edata, edata - KERNBASE);
    // printf("  end    %08x (virt)  %08x (phys)\n", end, end - KERNBASE);
    // printf("Kernel executable memory footprint: %dKB\n",
    //     ROUNDUP(end - entry, 1024) / 1024);
    printf("mon_kerninfo not implemented\n");
    return 0;
}

int
mon_backtrace(int argc, char **argv, struct Trapframe *tf)
{
    // // Your code here.
    // uint32_t ebp = read_ebp();
    // uint32_t *arg = (uint32_t *)ebp;
    // struct Eipdebuginfo info;

    // do {
    //     debuginfo_eip(arg[1], &info);
    //     printf("ebp %08x eip %08x args %08x %08x %08x %08x %08x\n",
    //             arg[0], arg[1], arg[2], arg[3], arg[4], arg[5], arg[6]);
    //     printf("\tfile %s:", info.eip_file);
    //     printf("%d:", info.eip_line);
    //     printf(" %.*s\n", info.eip_fn_namelen, info.eip_fn_name);
    //     arg = (uint32_t *)arg[0];
    // }while(arg[0] != 0);
    printf("mon_backtrace not implemented\n");
    return 0;
}

/***** Kernel monitor command interpreter *****/

#define WHITESPACE "\t\n "
#define MAXARGS 16

static int
runcmd(char *buf, struct Trapframe *tf)
{
    int argc;
    char *argv[MAXARGS];
    int i;

    // Parse the command buffer into whitespace-separated arguments
    argc = 0;
    argv[argc] = 0;
    while (1) {
        // gobble whitespace
        while (*buf && strchr(WHITESPACE, *buf))
            *buf++ = 0;
        if (*buf == 0)
            break;

        // save and scan past next arg
        if (argc == MAXARGS-1) {
            printf("Too many arguments (max %d)\n", MAXARGS);
            return 0;
        }
        argv[argc++] = buf;
        while (*buf && !strchr(WHITESPACE, *buf))
            buf++;
    }
    argv[argc] = 0;

    // Lookup and invoke the command
    if (argc == 0)
        return 0;
    for (i = 0; i < ARRAY_SIZE(commands); i++) {
        if (strcmp(argv[0], commands[i].name) == 0)
            return commands[i].func(argc, argv, tf);
    }
    printf("Unknown command '%s'\n", argv[0]);
    return 0;
}

void
monitor(struct Trapframe *tf)
{
    char *buf;

    printf("Welcome to the JOS kernel monitor!\n");
    printf("Type 'help' for a list of commands.\n");


    while (1) {
        buf = readline("K> ");
        if (buf != NULL)
            if (runcmd(buf, tf) < 0)
                break;
    }
}

