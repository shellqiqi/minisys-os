#include <inc/asm/asm.h>
#include <inc/pmap.h>
#include <inc/env.h>
#include <inc/printf.h>
#include <inc/traps.h>
#include <inc/tlbop.h>
#include <drivers/leds.h>

void mips_init()
{
	printf("init.c:\tmips_init() is called\n");
	mips_tlbinvalall ();
	// Lab 2 memory management initial 0x401000,ization functions
	mips_detect_memory();
	mips_vm_init();
	page_init();
    // page_check();

	env_init();
    printf("env init sucessed !\n");

	/*you can create some processes(env) here. in terms of binary code, please refer current directory/code_a.c
	 * code_b.c*/
	/*you may want to create process by MACRO, please read env.h file, in which you will find it. this MACRO is very
	 * interesting, have fun please*/

	ENV_CREATE(mytest);

	trap_init();
	kclock_init();

	while (1)
        monitor(NULL);

	panic("init.c:\tend of mips_init() reached!");
}

void ddelay() {
	volatile u32 j;
	for (j = 0; j < 1000000; ++j);
}

void bcopy(const void *src, void *dst, size_t len)
{
	void *max;

	max = dst + len;
	int iii = 0;
	// copy machine words while possible
	while (dst + 3 < max) {
		if (iii++ < 5) printf("bcopy dst %x %x\n", dst, *(int *)src);
		*(int *)dst = *(int *)src;
		dst += 4;
		src += 4;
	}

	// finish remaining 0-3 bytes
	while (dst < max) {
		*(char *)dst = *(char *)src;
		dst += 1;
		src += 1;
	}
}

void bzero(void *b, size_t len)
{
	void *max;

	max = b + len;

	//printf("init.c:\tzero from %x to %x\n",(int)b,(int)max);

	// zero machine words while possible

	while (b + 3 < max) {
		*(int *)b = 0;
		b += 4;
	}

	// finish remaining 0-3 bytes
	while (b < max) {
		*(char *)b++ = 0;
	}

}
