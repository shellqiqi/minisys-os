/*
 * Copyright (C) 2001 MontaVista Software Inc.
 * Author: Jun Sun, jsun@mvista.com or jsun@junsun.net
 *
 * This program is free software; you can redistribute  it and/or modify it
 * under  the terms of  the GNU General  Public License as published by the
 * Free Software Foundation;  either version 2 of the  License, or (at your
 * option) any later version.
 *
 */

#include <inc/printf.h>
#include <inc/pmap.h>
#include <kern/console.h>

#include <drivers/seven_seg.h>
#include <drivers/timer.h>
#include <drivers/leds.h>

#include <mips/cpu.h>

void print_banner();

int main()
{
    set_leds(0x5555); // let leds show 0x5555 when startup

    // set up interrupts
    // Clear boot interrupt vector bit in Coprocessor 0 Status register
    mips32_bicsr (SR_BEV);

    // Set master interrupt enable bit, as well as individual interrupt
    // enable bits in Coprocessor 0 Status register
    mips32_bissr (SR_IE | SR_HINT0 | SR_HINT1 | SR_HINT2 | SR_HINT3 | SR_HINT4);

    // Enable interrupt
    // Can't get interrupt until after we do this
    asm ("ei");

    // Initialize the console.
    // Can't call printf until after we do this!
    cons_init();
    // Print banner when start up.
    print_banner();

    // Enable seven segment digital tubes
    init_seven_seg();
    enable_all_seven_seg();

    // Enable timer
    // init_timer();

	printf("main.c:\tmain is start ...\n");

	mips_init();
	panic("main is over is error!");

	return 0;
}

void print_banner() {
    printf("\n");
    printf("\n");
    printf("\n");

    printf("    __  ____       _                    ____  _____\n");
    printf("   /  |/  (_)___  (_)______  _______   / __ \\/ ___/\n");
    printf("  / /|_/ / / __ \\/ / ___/ / / / ___/  / / / /\\__ \\ \n");
    printf(" / /  / / / / / / (__  ) /_/ (__  )  / /_/ /___/ / \n");
    printf("/_/  /_/_/_/ /_/_/____/\\__, /____/   \\____//____/  \n");
    printf("                      /____/                       \n");

    printf("\n");
}
